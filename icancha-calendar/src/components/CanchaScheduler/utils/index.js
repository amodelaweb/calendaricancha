import moment from 'moment';

export const filterDateArray = (arrayFields, activeDate) => {
    let inDayArr = arrayFields.map(field => {
        let fieldsInActiveDay = field
            .rsvp
            .filter(timeFrame => moment(timeFrame.fecha, "YYYY-MM-DD").isSame(moment(activeDate), 'date'))

        return {
            ...field,
            rsvp: fieldsInActiveDay
        };
    });

    return inDayArr;
}

export const sameDay = (day1, day2) => (moment(day1).isSame(moment(day2), 'date'));

export const reduceDaysToDate = (date, days) => (moment(date).subtract(days, 'days').toDate());

export const addDaysToDate = (date, days) => (moment(date).add(days, 'days').toDate());