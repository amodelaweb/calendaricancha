import {Row} from 'antd';
import "antd/dist/antd.css";
import moment from 'moment';
import 'moment/locale/es';
import React, {useState} from 'react';
import ActiveDayComponent from './ActiveDayComponent';
import {COLOR_LIST} from './constants/ColorsConstants';
import ScheduleComponent from './ScheduleComponent';
import GlobalStyle from './styles/GlobalStyle';
import {filterDateArray, sameDay} from './utils';

moment.updateLocale('es', {
    months: [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ],
    weekdays: [
        "Domingo",
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado"
    ]
});

const CanchaScheduler = ({fieldsArray}) => {
    const todayDate = new Date();

    const [state,
        setState] = useState({
        activeDay: todayDate,
        fieldsArray: filterDateArray(fieldsArray, todayDate),
        colorMap: new Map(),
        colorList: [...COLOR_LIST]
    });

    const changeDateCallBack = newDate => {

        if (!sameDay(newDate, state.activeDay)) {
            setState(prevState => ({
                ...prevState,
                activeDay: newDate,
                fieldsArray: filterDateArray(fieldsArray, newDate),
                colorMap: new Map(),
                colorList: [...COLOR_LIST]
            }));
        }

    };

    const nextArrowCallBack = _ => {
        if (state.fieldsArray[1].nombre !== fieldsArray[0].nombre) {
            let newArr = state.fieldsArray;
            newArr.push(state.fieldsArray[0])
            newArr.shift();
            setState(prevState => ({
                ...prevState,
                fieldsArray: newArr
            }));
        }
    };

    const beforeArrowCallBack = _ => {
        if (state.fieldsArray[state.fieldsArray.length - 1].nombre !== fieldsArray[fieldsArray.length - 1].nombre) {
            let newArr = state.fieldsArray;
            newArr.unshift(state.fieldsArray[state.fieldsArray.length - 1])
            newArr.pop();
            setState(prevState => ({
                ...prevState,
                fieldsArray: newArr
            }));
        }
    };

    const addNewColorToMapCallBack = (sourceId, color) => {
        if (!state.colorMap.has(sourceId)) {
            let newmap = state.colorMap;
            newmap.set(sourceId, color);
            setState(prevState => ({
                ...prevState,
                colorMap: newmap
            }));
        }
    }

    return (
        <React.Fragment>
            <Row type="flex" justify="start" align="middle">
                <ActiveDayComponent
                    activeDate={state.activeDay}
                    changeDateCallBack={changeDateCallBack}
                    leftArrowCallBack={beforeArrowCallBack}
                    rightArrowCallBack={nextArrowCallBack}/>
            </Row>
            <Row justify="center" align="middle">
                <ScheduleComponent
                    activeDay={state.activeDay}
                    fieldsArray={state.fieldsArray}
                    colorMap={state.colorMap}
                    addNewColorToMapCallBack={addNewColorToMapCallBack}/>
            </Row>
            < GlobalStyle/>
        </React.Fragment>
    );

}

export default CanchaScheduler;