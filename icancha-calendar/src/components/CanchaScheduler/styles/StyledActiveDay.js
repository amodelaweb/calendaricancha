import styled from 'styled-components';

const StyledActiveDay = styled.div `

    width: 434px;
    height: 60px;
    display: inline-flex;
    align-items: flex-start;

    background: #FFFFFF;
    border: 1.25px solid #E9E9E9;
    box-sizing: border-box;
    border-radius: 3.75px;

    @media (max-width: 490px) {
        max-width: 250px;
        min-height: 90px;
    }

    .text {
        color: #000000;
        height: 30px;
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-size: 26px;
        line-height: 30px;

        margin-left: 21px;
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .iconbox {
        min-width: 87px;
        min-height: 60px;
        background: #00E676;
        border: 1.25px solid #E9E9E9;
        box-sizing: border-box;
        border-radius: 0px 3.75px 3.75px 0px;
        padding: auto;
        margin-left: auto;

        display: flex;
        align-items: center;
        justify-content: center;

        @media (max-width: 490px) {
            min-height: 90px;
        }
    }
`;

export default StyledActiveDay;