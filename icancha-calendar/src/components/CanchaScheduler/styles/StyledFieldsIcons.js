import styled from 'styled-components';

const StyledFieldsIcons = styled.div`
    vertical-align: middle;
    position: relative;
    text-align: end;
    display: inline-block;
    margin-left: 35%;
`;

export default StyledFieldsIcons;