import styled from 'styled-components';

const StyledHeader = styled.div`
    padding-left: 5%;
    display: inline-flex;
    min-width: 100%;

    .tooltip-cancha {
        font-weight: 500 !important;
        font-size: 11px !important;
        line-height: 13px !important;
        background: #FFFFFF !important;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1) !important;
        border-radius: 5px !important;
    }
`;

export default StyledHeader;