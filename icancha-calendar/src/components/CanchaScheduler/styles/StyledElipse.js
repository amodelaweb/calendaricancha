import styled from 'styled-components';

const StyledElipse = styled.div`
    width: 6px;
    height: 6px;
    left: 176px;
    top: 271px;
    border-radius: 50%;
    margin: 8px;
    align-items: center;
    vertical-align: middle;

    display: inline-block;
    margin-top: 5px;
    /* Green Money */

    background: #00E676;
`;

export default StyledElipse;