import styled from 'styled-components';

export const Table = styled.table `
    align-items: stretch;
    height: 100%;
    
    background: #FFFFFF;
    border: 1px solid #E9E9E9;
    box-sizing: border-box;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    border-collapse: unset;
    display: inline-block;
    margin-left: 0.5%;
`;

export const THead = styled.thead `
`;

export const TH = styled.th `
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 28px;
    text-align: ${props => props.isBook
    ? 'start'
    : 'center'};
    /* Blue Leagues Under the Sea */
    color: #1B1464;
    padding: 0 !important;
    height: 61px;
    border-bottom: ${props => props.isBook
        ? '1px solid'
        : '1px solid #D2D2D2'};
    
    ${props => props.isBook
            ? () => `
    border-image-source: linear-gradient(to left, red, purple);;
    border-image-slice: 1;
    `
            : ``};
    overflow-x: hidden;

    .schedule-butt {
        display: inline-flex;
        width: 49px;
        height: 28px;
        /* Gray 1 */
        margin-left: auto;
        background: #EDEDED;
        border-radius: 16.8116px 0px 0px 16.8116px;
    }

    .book-img {
        margin: auto;
    }
`;

export const TBody = styled.tbody `
    border-radius: 5px;
`;

export const TR = styled.tr `
    border-radius: 5px;
    height: 61px;
`;

export const TD = styled.td `
    text-align: center; 
    vertical-align: middle;
    min-width: ${props => props.isBook
    ? '333.194px'
    : '0px'};
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    height: 61px;
    font-size: 18px;
    min-width: 80px;
    line-height: 21px;
    /* identical to box height */
    color: #000000;

    border-bottom:${props => !props.isLast
        ? '1px solid #D2D2D2'
        : 'none'} ;
    
    @media (max-width: 410px) {
        max-width: 220px;
    }
`;