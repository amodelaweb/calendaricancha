import styled from 'styled-components';

const StyledTimeSlot = styled.div`
    /*  Block **/
    background: ${props => props.background ? props.background : 'white'};
    border-radius: 3px;
    text-align: left;
    height: 48px;
    width: 319px;
    padding: 3%;
    /*  Text **/
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 28px;

    color: white;
`;

export default StyledTimeSlot;