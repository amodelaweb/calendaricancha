import styled from 'styled-components';

const StyledFieldsContainer = styled.div`
    height: 100%;
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
`;

export default StyledFieldsContainer;