import styled from 'styled-components';

const StyledSchedulerBody = styled.div`
      margin-top: 0.8%;
      display: grid;
      overflow-y: hidden;
      grid-template-columns: min-content auto;
      grid-auto-rows: minmax(100px, auto);
`;

export default StyledSchedulerBody;