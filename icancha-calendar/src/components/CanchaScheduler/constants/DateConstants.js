export const DATE_PICKER_FORMAT = "dddd D [de] MMMM";
export const SIMPLE_HOUR = "hh A";
export const ACTIVE_LOCALE = "es";
export const MIN_DATE = 30;
export const MAX_DATE = 30;