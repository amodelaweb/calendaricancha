import moment from 'moment';
import React from 'react';
import MdAlarm from 'react-ionicons/lib/MdAlarm';
import Moment from 'react-moment';
import book from '../../assets/icons/book.svg';
import { ACTIVE_LOCALE, SIMPLE_HOUR } from './constants/DateConstants';
import StyledFieldsContainer from './styles/StyledFieldsContainer';
import StyledHeader from './styles/StyledHeader';
import StyledSchedulerBody from './styles/StyledSchedulerBody';
import { Table, TBody, TD, TH, THead, TR } from './styles/StyledTable';
import TimeSlotComponent from './TimeSlotComponent';
import ReactTooltip from "react-tooltip";

const ScheduleComponent = ({colorMap, addNewColorToMapCallBack, fieldsArray, activeDay}) => {

    const buildHourRows = _ => {
        let hourCounter = moment("600", "hmm");
        let rows = [];
        for (let i = 0; i <= 18; i++) {
            rows.push(
                <TR key={`tr-${i}`}>
                    <TD
                        key={`td-${i}`}
                        isLast={i < 18
                        ? false
                        : true}>
                        <Moment
                            format={SIMPLE_HOUR}
                            date={hourCounter.toDate()}
                            locale={ACTIVE_LOCALE}/>
                    </TD>
                </TR>
            );
            hourCounter.add(1, 'hours');
        }

        return rows;
    }

    const buildListOfFields = _ => {
        let fieldSchedule = [];
        fieldsArray.forEach(field => {
            let tableMapEvent = new Map();
            field
                .rsvp
                .forEach(book => {
                    tableMapEvent.set(book.hora, book);
                })

            let tableRows = []
            let hourCounter = moment(activeDay).set({hour: 6, minute: 0});

            for (let i = 0; i <= 18; i++) {
                tableRows.push(
                    <TR key={`tr-${field.idcancha}-${i}`}>
                        <TD
                            key={`td-${field.idcancha}-${i}`}
                            isLast={i < 18
                            ? false
                            : true}>
                            <TimeSlotComponent
                                colorMap={colorMap}
                                addNewColorToMapCallBack={addNewColorToMapCallBack}
                                timeStamp={hourCounter.toDate()}
                                customKey={`ts-${field.idcancha}-${i}`}
                                data={tableMapEvent.get(hourCounter.format("HH:mm"))}
                                idCancha={field.idcancha}/>
                        </TD>
                    </TR>
                );
                hourCounter.add(1, 'hours');
            }
            fieldSchedule.push(
                <Table key={`table-${field.nombre}`}>
                    <THead>
                        <TR>
                            <TH isBook={true}>
                                <StyledHeader>
                                    <div>
                                        <label>{field.nombre}</label>
                                    </div>
                                    <div className="schedule-butt" data-tip="React-tooltip">
                                        <img src={book} alt="book" className="book-img"/>
                                    </div>
                                    <ReactTooltip className='tooltip-cancha' place="top" type="light" effect="float">
                                        Cronograma Cancha
                                    </ReactTooltip>
                                </StyledHeader>
                            </TH>
                        </TR>
                    </THead>
                    <TBody>
                        {tableRows}
                    </TBody>
                </Table>
            );
        });
        return fieldSchedule;
    }

    return (
        <StyledSchedulerBody>
            <StyledFieldsContainer>
                <Table>
                    <THead>
                        <TR>
                            <TH>
                                <MdAlarm color="#00E676" fontSize="32px"/>
                            </TH>
                        </TR>
                    </THead>
                    <TBody>
                        {buildHourRows()}
                    </TBody>
                </Table>
            </StyledFieldsContainer>
            <StyledFieldsContainer>
                {buildListOfFields()}
            </StyledFieldsContainer>
        </StyledSchedulerBody>
    )
}

export default ScheduleComponent;