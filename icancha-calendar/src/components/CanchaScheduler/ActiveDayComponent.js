import {Col} from 'antd';
import mergeClassNames from 'merge-class-names';
import React, {useState} from 'react';
import Calendar from 'react-calendar';
import 'react-date-picker';
import Fit from 'react-fit';
import IosArrowDropleft from 'react-ionicons/lib/IosArrowDropleft';
import IosArrowDropright from 'react-ionicons/lib/IosArrowDropright';
import Moment from 'react-moment';
import {ACTIVE_LOCALE, DATE_PICKER_FORMAT, MAX_DATE, MIN_DATE} from './constants/DateConstants';
import StyledActiveDay from './styles/StyledActiveDay';
import StyledCalendarContext from './styles/StyledCalendarContext';
import {addDaysToDate, reduceDaysToDate} from './utils';
import calendar from '../../assets/icons/icon-calendar.svg';

const baseClassName = 'react-date-picker';
const className = `${baseClassName}__calendar`;

const ActiveDayComponent = ({activeDate, changeDateCallBack, leftArrowCallBack, rightArrowCallBack}) => {

    const [state,
        setState] = useState({isOpen: false});

    const changeOpenState = () => {
        setState(prevState => ({
            ...prevState,
            isOpen: !prevState.isOpen
        }));
    }

    const changeStateAndClose = (value) => {
        changeDateCallBack(value);
        changeOpenState();
    }

    const CalendarContext = () => (
        <Fit>
            <StyledCalendarContext
                className={mergeClassNames(className, `${className}--${state.isOpen
                ? 'open'
                : 'closed'}`)}>
                <Calendar
                    value={activeDate}
                    locale="es"
                    onChange={changeStateAndClose}
                    minDate={reduceDaysToDate(new Date(), MIN_DATE)}
                    maxDate={addDaysToDate(new Date(), MAX_DATE)}/>
            </StyledCalendarContext>
        </Fit>
    )

    return (
        <React.Fragment >
            <Col span={8}>
                <StyledActiveDay onClick={changeOpenState}>
                    <label className="text">
                        <Moment format={DATE_PICKER_FORMAT} date={activeDate} locale={ACTIVE_LOCALE}/>
                    </label>
                    <div className="iconbox">
                        <img src={calendar} alt="calendar_logo"/>
                    </div>
                </StyledActiveDay>
                <CalendarContext/>
            </Col>
            <Col
                span={8}
                offset={8}
                style={{
                display: 'flex',
                justifyContent: 'flex-end',
                paddingRight: '0.8%'
            }}>
                <IosArrowDropleft color="#8A2BE2" fontSize="26px" onClick={leftArrowCallBack}/>
                <IosArrowDropright
                    color="#8A2BE2"
                    fontSize="26px"
                    onClick={rightArrowCallBack}/>
            </Col>
        </React.Fragment>
    )
}

export default ActiveDayComponent;