import React from 'react';
import { COLOR_LIST } from './constants/ColorsConstants';
import { clickBlankSlot, clickBookSlot } from './services';
import StyledTimeSlot from './styles/StyledTimeSlot';

const TimeSlotComponent = ({
    data,
    timeStamp,
    customKey,
    colorMap,
    addNewColorToMapCallBack,
    idCancha
}) => {

    const selectColor = () => {
        if (data) {
            if (colorMap.has(data.idorigen)) {
                return colorMap.get(data.idorigen);
            } else {
                let newColor = COLOR_LIST[Math.floor(Math.random() * COLOR_LIST.length)];
                addNewColorToMapCallBack(data.idorigen, newColor);
                return newColor;
            }
        }
        return 'white';
    }
    const handleOnClick = (e) => {
        if (!data) {
            clickBlankSlot(timeStamp);
        } else {
            clickBookSlot({
                ...data,
                idcancha: idCancha
            });
        }
    }

    return (
        <StyledTimeSlot
            background={() => selectColor()}
            onClick={handleOnClick}
            key={customKey}>
            {data
                ? data.nombre
                : ""}
        </StyledTimeSlot>
    );
}

export default TimeSlotComponent;