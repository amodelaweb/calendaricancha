import React from 'react';
import CanchaScheduler from '../CanchaScheduler';

function App() {
    let arrayCanchas = [
        {
            "idcancha": 1,
            "nombre": "Cancha 1",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-15",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-13",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 2,
            "nombre": "Cancha 2",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-16",
                    "nombre": "F En el chat",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-12",
                    "nombre": "Jaime daza",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 3,
            "nombre": "Cancha 3",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-15",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-15",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 4,
            "nombre": "Cancha 4",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-16",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-15",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 5,
            "nombre": "Cancha 5",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-17",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-13",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 6,
            "nombre": "Cancha 6",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-15",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-12",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }, {
            "idcancha": 7,
            "nombre": "Cancha 7",
            "rsvp": [
                {
                    "hora": "06:00",
                    "fecha": "2020-02-15",
                    "nombre": "Lucas castillo",
                    "monto": 12000.00,
                    "idorigen": 2
                }, {
                    "hora": "07:00",
                    "fecha": "2020-02-18",
                    "nombre": "Juan Cabellero",
                    "monto": 13000.00,
                    "idorigen": 1
                }
            ]
        }
    ]

    return (<CanchaScheduler fieldsArray={arrayCanchas}/>);
}

export default App;
